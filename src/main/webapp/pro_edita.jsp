<%-- 
    Document   : modifica registro
    Created on : Apr 24, 2021, 4:54:19 PM
    Author     : danielmunoz
--%>

<%@page import="root.entity.Proveedores"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Proveedores pro_editar = (Proveedores) request.getAttribute("edita_proveedor");
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            form {
                /* Centrar el formulario en la página */
                margin: 0 auto;
                width: 500px;
                /* Esquema del formulario */
                padding: 1em;
                border: 1px solid #CCC;
                border-radius: 1em;
            }
        </style>
        <title>Home Apps</title>
    </head>
    <body>   
        <h3 style="text-align: center">EDITA REGISTRO DE PROVEEDOR</h3>
        <form name="form" action="ControllerApp" method="POST">
            ID Proveedor : <input type="text" name="pro_id_proveedor" id="pro_id_proveedor" value="<%=pro_editar.getProIdProveedor()%>" size=5 /><br>
            Nombre    : <input type="text" name="pro_nombre" id="pro_nombre" value="<%=pro_editar.getProNombre()%>" size="50"/><br>
            Apellido  : <input type="text" name="pro_apellido" id="pro_apellido" value="<%=pro_editar.getProApellido()%>" size="50"/><br>
            Telefono  : <input type="text" name="pro_telefono" id="pro_telefono" value="<%=pro_editar.getProTelefono()%>" size="50"/><br>
            Dirección : <input type="text" name="pro_direccion" id="pro_direccion" value="<%=pro_editar.getProDireccion()%>" size="50"/><br>

            <div >
            <center>
                <br>
                <button type="submit" name="action" value="guardar" > Guardar Registro </button>
                <button type="submit" name="action" value="editar"> Editar Registros </button>
            </center>
            </div>
        </form>

    </body>
</html>

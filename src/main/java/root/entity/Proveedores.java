
package root.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author danielmunoz
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Proveedores.findAll", query = "SELECT p FROM Proveedores p"),
    @NamedQuery(name = "Proveedores.findByProIdProveedor", query = "SELECT p FROM Proveedores p WHERE p.proIdProveedor = :proIdProveedor"),
    @NamedQuery(name = "Proveedores.findByProNombre", query = "SELECT p FROM Proveedores p WHERE p.proNombre = :proNombre"),
    @NamedQuery(name = "Proveedores.findByProApellido", query = "SELECT p FROM Proveedores p WHERE p.proApellido = :proApellido"),
    @NamedQuery(name = "Proveedores.findByProTelefono", query = "SELECT p FROM Proveedores p WHERE p.proTelefono = :proTelefono"),
    @NamedQuery(name = "Proveedores.findByProDireccion", query = "SELECT p FROM Proveedores p WHERE p.proDireccion = :proDireccion")})
public class Proveedores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "pro_id_proveedor", nullable = false, length = 2147483647)
    private String proIdProveedor;
    @Size(max = 2147483647)
    @Column(name = "pro_nombre", length = 2147483647)
    private String proNombre;
    @Size(max = 2147483647)
    @Column(name = "pro_apellido", length = 2147483647)
    private String proApellido;
    @Size(max = 2147483647)
    @Column(name = "pro_telefono", length = 2147483647)
    private String proTelefono;
    @Size(max = 2147483647)
    @Column(name = "pro_direccion", length = 2147483647)
    private String proDireccion;

    public Proveedores() {
        
    }

    public Proveedores(String proIdProveedor) {
        this.proIdProveedor = proIdProveedor;
    }

    public String getProIdProveedor() {
        return proIdProveedor;
    }

    public void setProIdProveedor(String proIdProveedor) {
        this.proIdProveedor = proIdProveedor;
    }

    public String getProNombre() {
        return proNombre;
    }

    public void setProNombre(String proNombre) {
        this.proNombre = proNombre;
    }

    public String getProApellido() {
        return proApellido;
    }

    public void setProApellido(String proApellido) {
        this.proApellido = proApellido;
    }

    public String getProTelefono() {
        return proTelefono;
    }

    public void setProTelefono(String proTelefono) {
        this.proTelefono = proTelefono;
    }

    public String getProDireccion() {
        return proDireccion;
    }

    public void setProDireccion(String proDireccion) {
        this.proDireccion = proDireccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proIdProveedor != null ? proIdProveedor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proveedores)) {
            return false;
        }
        Proveedores other = (Proveedores) object;
        if ((this.proIdProveedor == null && other.proIdProveedor != null) || (this.proIdProveedor != null && !this.proIdProveedor.equals(other.proIdProveedor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.entity.Proveedores[ proIdProveedor=" + proIdProveedor + " ]";
    }
    
}

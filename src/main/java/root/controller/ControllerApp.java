/**
 *
 * @author danielmunoz
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.dao.ProveedoresJpaController;
import root.dao.exceptions.NonexistentEntityException;
import root.entity.Proveedores;


@WebServlet(name = "ControllerApp", urlPatterns = {"/ControllerApp"})
public class ControllerApp extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerApp</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerApp at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");

//  *** INICIO ACCION BOTON GUARDAR ***
        if (action.equals("guardar")) {          
            try {
                /**
                 * Creación de variables para registros que vienen del JSP
                 */
                String pro_id_proveedor = request.getParameter("pro_id_proveedor");
                String pro_nombre = request.getParameter("pro_nombre");
                String pro_apellido = request.getParameter("pro_apellido");
                String pro_telefono = request.getParameter("pro_telefono");
                String pro_direccion = request.getParameter("pro_direccion");

                /**
                 * Intanciación de la clase Entity y seteo de valores hacia la
                 * DB
                 */
                Proveedores proveedor = new Proveedores();
                proveedor.setProIdProveedor(pro_id_proveedor);
                proveedor.setProNombre(pro_nombre);
                proveedor.setProApellido(pro_apellido);
                proveedor.setProTelefono(pro_telefono);
                proveedor.setProDireccion(pro_direccion);

                /**
                 * Instanciación de la clase DAO para persistir datos en la DB
                 */
                ProveedoresJpaController miDao = new ProveedoresJpaController();

                /**
                 * Guarda los datos ingresados
                 */
                miDao.create(proveedor);

                /**
                 * Contención de errores
                 */
            } catch (Exception ex) {
                Logger.getLogger(ControllerApp.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
//  *** FIN BOTON GUARDAR ***

//  *** INICIO ACCION BOTON LISTAR REGISTRO PROVEEDORES ***
        if (action.equals("listar")) {
            
            ProveedoresJpaController miDao = new ProveedoresJpaController();

            List<Proveedores> pro_lista = miDao.findProveedoresEntities();
            
            request.setAttribute("pro_registros", pro_lista);
            request.getRequestDispatcher("pro_registros.jsp").forward(request, response);
            
        }
//  *** FIN BOTON LISTAR ***

//  *** INICIO ACCION BOTON EDITAR ***
        if (action.equals("editar")){
            
                String pro_id_proveedor = request.getParameter("edita_proveedor");
                
                ProveedoresJpaController miDao = new ProveedoresJpaController();
//                miDao.destroy(pro_id_proveedor);
          
        }
//  *** FIN BOTON EDITAR *** 
 
//  *** INICIO ACCION BOTON ELIMINAR ***
        if (action.equals("eliminar")){

            try {
                String pro_id_proveedor = request.getParameter("pro_registros");
                
                ProveedoresJpaController miDao = new ProveedoresJpaController();
                miDao.destroy(pro_id_proveedor);
                
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(ControllerApp.class.getName()).log(Level.SEVERE, null, ex);
            }
          
        }
//  *** FIN BOTON ELIMINAR *** 
        
        
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
